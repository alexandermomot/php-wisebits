#!/usr/bin/env bash

php -i &> /dev/null

find_or_get_composer() {
    if [ ! -f ./composer.phar ]
    then
        echo "Composer found, installing dependencies..."
    else
        curl -sSO https://getcomposer.org/download/1.4.1/composer.phar
    fi
    php composer.phar install
}

# Composer installation

if [ $? != 0 ]
then
    echo "PHP interpreter not found in the system, please install it first..."
    echo "e.g. here: http://php.net/downloads.php#v5.6.30"
    exit 1
else
    VERSION=$(php -i | grep -m 1 'PHP Version')
    printf "%s\nInitializing composer and installing it's dependencies..." "$VERSION"
    find_or_get_composer
fi

set -e

# Environment set

if [ ! -f ./.env ]; then
    echo 'Creating .env file with configs'
    cp ./.env.dist ./.env
fi

# Codeception preparations

bin/codecept build
