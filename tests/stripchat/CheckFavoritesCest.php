<?php
namespace Wisetest;

use Wisetest\StripchatTester;
use Wisetest\Page\Stripchat\ModelPage;
use Wisetest\Page\Stripchat\MainPage;
use Wisetest\Page\Stripchat\LoginPage;
use Wisetest\Page\Stripchat\UserFavoritesPage;

/**
 * The suite gather the tests regarding user personal page actions
 * with favorites like: add models, remove models, etc.
 *
 * Class CheckFavoritesCest
 * @package Wisetest
 */
class CheckFavoritesCest
{
    /**
     * Delete from favorites button text to check during the test
     */
    const FAVORITES_DELETE_BTN_TEXT = 'Удалить из избранного';

    /**
     * User login as authorized user and
     * prepares page for work
     *
     * @param \Wisetest\StripchatTester $I
     */
    public function _before(StripchatTester $I)
    {
        $credentials = $I->getCredentials();

        $I->amOnPage(LoginPage::$URL);
        $I->agreeModalAgeOver18();

        (new LoginPage($I))
            ->authAs($credentials['login'], $credentials['password']);

        (new MainPage($I))
            ->waitForUserBecomesLoggedIn()
            ->waitForModelsToAppear();
    }

    /**
     * Testing ability of the user to add particular (random) model
     * to the favorites list
     *
     * @group ui
     * @param \Wisetest\StripchatTester $I
     */
    public function testAddToFavorites(StripchatTester $I)
    {
        $I->wantToTest('user can add model to favorites');

        $link = $I->findRandomModelLink();

        $I->amGoingTo('check that favorites button is working');

        (new MainPage($I))
            ->gotoModelChatPage($link)
            ->openModelProfile()
            ->addModelToFavorites();

        $modelName = $I->getModelName();

        // First check
        $I->see(self::FAVORITES_DELETE_BTN_TEXT, ModelPage::$addToFavoritesBtn);

        $I->amGoingTo('check that model were added to favorites page');

        $I->amOnPage(UserFavoritesPage::$URL);
        $I->waitForElement(UserFavoritesPage::$modelCardName);

        // Second check
        $I->seeRequiredModelInTheList();

        // Just to ensure that model exists on the page favorites
        $I->makeScreenshot('model-'.$modelName);
    }

    /**
     * Perform logout and clean favorites (all models recursively)
     *
     * @param \Wisetest\StripchatTester $I
     */
    public function _after(StripchatTester $I)
    {
        $I->amOnPage(UserFavoritesPage::$URL);
        $I->modelFavoritesCleanUp();
    }
}
