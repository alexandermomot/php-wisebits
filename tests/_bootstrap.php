<?php
use Codeception\Util\Autoload;

$BASEDIR = __DIR__ . DIRECTORY_SEPARATOR;

Autoload::addNamespace('Wisetest\Helper', $BASEDIR . '_support/Helper');
