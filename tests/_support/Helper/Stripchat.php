<?php
namespace Wisetest\Helper;

use Codeception\Module;
use Codeception\Module\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeOutException;
use Facebook\WebDriver\WebDriver as FacebookWebDriver;
use Facebook\WebDriver\WebDriverElement;
use Wisetest\Page\Stripchat\MainPage;
use Wisetest\Page\Stripchat\ModelPage;
use Wisetest\Page\Stripchat\UserFavoritesPage;

class Stripchat extends Module
{
    /**
     * @var WebDriver $wd codeception webdriver helper injection
     */
    public $wd;

    /**
     * @var string $modelName name of the current model
     */
    private $modelName;

    /**
     * Codeception inherit function, injects webdriver
     */
    public function _initialize()
    {
        $this->wd = $this->getModule('WebDriver');
    }

    /**
     * Click on modal window "i'm over 18 years old",
     * if the dialog appeared on the current screen
     *
     * @return $this
     */
    public function agreeModalAgeOver18()
    {
        /** @var FacebookWebDriver $driver */
        $driver = $this->wd->webDriver;

        $this->wd->waitForElementVisible(MainPage::$disclaimerAgreeBtn);
        $xpath = WebDriverBy::xpath(MainPage::$disclaimerAgreeBtn);

        /** @var WebDriverElement[] $disclaimerBtn */
        $disclaimerBtn = $driver->findElements($xpath);

        if ($disclaimerBtn) {
            $disclaimerBtn[0]->click();
        }

        return $this;
    }

    /**
     * Finds href of an model and trims it's domain from a link
     *
     * @return mixed
     */
    public function findRandomModelLink()
    {
        $models = $this->wd->grabMultiple(MainPage::$modelPreview, 'href');
        $modelLink = $models[array_rand($models)];
        $modelShortLink = str_replace(getenv('APPLICATION_STRIPCHAT_HOST'), '', $modelLink);
        return $modelShortLink;
    }

    /**
     * Returns ["login" => "john", "password" => "secret"]
     * for current user by reading env variables
     *
     * @return array
     */
    public function getCredentials()
    {
        return [
            'login' => getenv('STRIPCHAT_USER'),
            'password' => getenv('STRIPCHAT_PASS'),
        ];
    }

    /**
     * Returns model name as it mentions in the header on the model page
     *
     * @return string
     */
    public function getModelName()
    {
        $this->modelName = $this->wd->grabTextFrom(ModelPage::$modelName);
        return $this->modelName;
    }

    /**
     * Checks that model exists in the favorites list
     */
    public function seeRequiredModelInTheList()
    {
        $models = $this->wd->grabMultiple(UserFavoritesPage::$modelCardName);
        $this->assertContains($this->modelName, $models);
    }

    /**
     * Clean (remove) all models from the favorites
     */
    public function modelFavoritesCleanUp()
    {
        /** @var FacebookWebDriver $driver */
        $driver = $this->wd->webDriver;

        $this->wd->click(UserFavoritesPage::$editAndSaveFavBtn);
        $this->wd->waitForElementVisible(UserFavoritesPage::$removeCrossBtn);

        /** @var WebDriverElement[] $disclaimerBtn */
        $crossBtns = $driver->findElements(WebDriverBy::cssSelector(UserFavoritesPage::$crossesBtn));

        foreach ($crossBtns as $btn) {
            $btn->click();
        }

        $this->wd->click(UserFavoritesPage::$editAndSaveFavBtn);
    }
}
