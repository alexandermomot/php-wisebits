<?php
namespace Wisetest\Page\Gmail;

use \Wisetest\GmailTester;

class LoginPage
{
    // include url of current page
    public static $URL = '/';

    /** Declare UI map for this page here. CSS or XPath allowed. */
    public static $mailField = 'input[type="email"]';
    public static $passField = 'input[type="password"]';
    public static $identNextButton = "input#next";
    public static $passwNextButton = "input#signIn";

    public static $securityOptions = 'div[role="region"]';

    /** @var GmailTester $guys */
    protected $guy;

    /**
     * LoginPage constructor.
     * @param GmailTester $tester
     */
    public function __construct(GmailTester $tester)
    {
        $this->guy = $tester;
    }

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     * @param $param
     * @return string
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * Authorization method for user(s)
     * @param $email
     * @param $password
     * @return $this
     */
    public function loginAs($email, $password)
    {
        $guy = $this->guy;

        // Fill login and click next
        $guy->fillField(self::$mailField, $email);
        $guy->waitForElement(self::$identNextButton);
        $guy->click(self::$identNextButton);

        // Wait next field to appear
        $guy->waitForElement(self::$passField);
        $guy->waitForElementVisible(self::$passField);

        // Fill password form and submit
        $guy->fillField(self::$passField, $password);
        $guy->click(self::$passwNextButton);

        // Not so fast, lets wait for render
        $guy->wait(1);

        return $this;
    }

}
