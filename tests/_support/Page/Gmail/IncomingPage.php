<?php
namespace Wisetest\Page\Gmail;

use Wisetest\GmailTester;

class IncomingPage
{
    // include url of current page
    public static $URL = '/mail/u/0/#inbox';

    // Common interface block (e.g. new mail, refresh button, etc.)
    public static $newMailBtn = 'div[role=button][gh=cm]';
    public static $refreshMailBtn = '//div[@aria-label="Обновить"]';
    public static $mainControlBlock = '//div[@tabindex="0"]//div[@role="presentation"]';
    public static $mailsList = "input#next";

    // New mail popup block
    public static $popupTitle = '//h2/div[text()="Новое сообщение"]';
    public static $popupToText = '//textarea[@aria-label="Кому"]';
    public static $popupSubjectText = 'input[name=subjectbox]';
    public static $popupLetterText = '//div[@aria-label="Тело письма"]';
    public static $popupSendBtn = "//div[contains(@aria-label,'Отправить')]";

    // Visible elements alerts and confirms
    public static $alertLetterSend = 'span#link_vsm[role=link]';

    // Letters table
    public static $lettersTable = '//table[@id]//tr';
    public static $lettersLayer = '//div[@role="tabpanel"]';
    public static $letterSubject = '//table[@id]//tr/td[6]//span[1]';
    public static $letterBody = '//table[@id]//tr/td[6]//span[2]';

    // Inside letter
    public static $letterReplyButton = '//div[text()="Нажмите здесь, чтобы "]//span[text()="Ответить"]';
    public static $letterReplyText = '//div[@role="textbox"]';

    // Security ? Turned off
    public static $securityOptions = 'div[role="region"]';

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var GmailTester;
     */
    protected $I;

    public function __construct(GmailTester $I)
    {
        $this->I = $I;
    }

    /**
     * Click to create new letter and wait for popup appears
     * @return $this
     */
    public function createNewLetter()
    {
        $I = $this->I;
        $I->click(self::$newMailBtn);
        $I->waitForElementVisible(self::$popupTitle);
        return $this;
    }

    /**
     * Subject of the letter
     * @param $subject
     * @return $this
     */
    public function inputSubject($subject)
    {
        $I = $this->I;
        $I->fillField(self::$popupSubjectText, $subject);
        return $this;
    }

    /**
     * Recipient email address, he should receive the email
     * @param $email
     * @return $this
     */
    public function inputWho($email)
    {
        $I = $this->I;
        $I->fillField(self::$popupToText, $email);
        return $this;
    }

    /**
     * Input text to the letter body
     * @param $text
     * @return $this
     */
    public function inputLetter($text)
    {
        $I = $this->I;
        $I->fillField(self::$popupLetterText, $text);
        return $this;
    }

    /**
     * Click send letter and wait for approve alert
     * @return $this
     */
    public function sendLetter()
    {
        $I = $this->I;
        $I->click(self::$popupSendBtn);
        $I->waitForElementVisible(self::$alertLetterSend);
        return $this;
    }

    /**
     * Open letter with number
     * @param int $number
     * @return $this
     */
    public function openLetter($number = 1)
    {
        $I = $this->I;
        $I->moveMouseOver(self::$lettersLayer);
        $I->click("(" . self::$letterSubject . ")[$number]");
        $I->waitForElementVisible(self::$letterReplyButton);
        return $this;
    }

    public function replyWithText($text)
    {
        $I = $this->I;
        $I->click(self::$letterReplyButton);
        $I->waitForElementVisible(self::$letterReplyText);
        $I->fillField(self::$letterReplyText, $text);
        $I->click(self::$popupSendBtn);
        return $this;
    }

    /**
     * Performs transition to the inbox page
     * @return $this
     */
    public function checkInbox()
    {
        $I = $this->I;
        $I->wait(2);
        $I->amOnPage(self::$URL);
        return $this;
    }

    /**
     * Waits for a letter from someone, checks table for changes periodically,
     * for example $count = 5, $pool = 2 means look up 5 times with every 2 seconds,
     * is there any new letters with $subject and $body (if set)
     * @param string $subject mandatory: subject of the letter
     * @param string $body mandatory: letter body
     * @param int $count optional: count of seconds to pool
     * @param int $pool optional: pooling interval between checks
     * @return $this
     */
    public function waitForLetterWith($subject, $body, $count = 5, $pool = 1)
    {
        $I = $this->I;
        for ($i = 0; $i < $count; $i++) {
            $I->waitForElementVisible(self::$mainControlBlock);
            $I->moveMouseOver(self::$mainControlBlock);
            $I->waitForElementVisible(self::$refreshMailBtn);
            $I->click(self::$refreshMailBtn);
            $I->wait($pool);
            $letter = [
                'subjects' => $I->grabMultiple(self::$letterSubject),
                'bodies'   => $I->grabMultiple(self::$letterBody),
            ];
            if (in_array($subject, $letter['subjects'])) {
                if (preg_grep("/\b.*$body.*\b/i", $letter['bodies'])) {
                    break;
                }
            }
        }
        return $this;
    }

}
