<?php
namespace Wisetest\Page\Market;

use Wisetest\MarketTester;

class CatalogPage
{
    /** @var string $URL Base url of the page */
    public static $URL = '/catalog/54726/list';

    /** @var MarketTester; */
    protected $I;

    /** Declare UI map for this page here. CSS or XPath allowed */
    public static $productTitle = '//h3/a[@title]/span';
    public static $productMutator = '//h3/a[@title]/span[text()="%s"]';
    public static $nextPageBtn = '//a[@href]/span[text()="Вперед"]/..';
    public static $productTable = '//div[contains(@class,"snippet-list")]/div[@data-id]/..';
    public static $spinner = 'div.spin2';

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     * @param $param
     * @return string
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public function __construct(MarketTester $I)
    {
        $this->I = $I;
    }

    /**
     * @return $this
     */
    public function clickNextButton()
    {
        $I = $this->I;
        $I->click(self::$nextPageBtn);
        return $this;
    }

    public function waitPageToLoad()
    {
        $I = $this->I;

        // Waiting for ajax finished and render
        $I->waitForJS("return $.active == 0;", 15);
        $I->waitForElementNotVisible(self::$spinner);

        // Formal waiting for an elements to interact with
        $I->waitForElementVisible(self::$productTable, 5);

        return $this;
    }

    /**
     * @param $product
     * @return $this
     */
    public function scrollToProductView($product)
    {
        $I = $this->I;
        $I->scrollTo(sprintf(self::$productMutator, $product));
        return $this;
    }

}
