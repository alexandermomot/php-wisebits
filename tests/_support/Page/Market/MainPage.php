<?php
namespace Wisetest\Page\Market;

use Wisetest\MarketTester;
use Wisetest\Page\Market\CatalogPage;

class MainPage
{
    /** @var string $URL Base url of the page */
    public static $URL = '/';

    /** @var MarketTester; */
    protected $I;

    /** Declare UI map for this page here. CSS or XPath allowed */
    public static $searchInput = 'input#header-search';
    public static $popup = [
        'block' => 'div.popup__content',
        'item' => '//li[contains(@class,"suggest2-item")]',
        'linkMutator' => '/a[contains(text(), "%s")]',
    ];

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     * @param $param
     * @return string
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public function __construct(MarketTester $I)
    {
        $this->I = $I;
    }

    /**
     * Input string $text into the search field and
     * waits for the dropdown list shows up,
     * e.g. "Nokia 3260"
     *
     * @param string $product
     * @return MainPage
     */
    public function searchFor($product)
    {
        $I = $this->I;
        $I->fillField(self::$searchInput, $product);
        return $this;
    }

    /**
     * Waits for dropdown list shows up, should
     * be used with searchFor method
     * @return MainPage
     */
    public function waitForPopupMenuShown()
    {
        $I = $this->I;
        $I->waitForElementVisible(self::$popup['block'], 15);
        return $this;
    }

    /**
     * Find and click to an item in the popup menu with
     * text partially match to the $entity
     * @param string $entity
     * @return $this
     */
    public function clickPopupMenuItemLike($entity)
    {
        $I = $this->I;
        $link = sprintf(self::$popup['linkMutator'], $entity);
        $I->click(self::$popup['item'] . $link);
        return $this;
    }

    /**
     * Input string $product into the search field and
     * waits for the page reload.
     *
     * @return CatalogPage
     */
    public function clickSearch()
    {
        return new CatalogPage($this->I);
    }

}
