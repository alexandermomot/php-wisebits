<?php
namespace Wisetest\Page\Stripchat;

use Wisetest\StripchatTester;

class ModelPage
{
    /**
     * @var string $URL of the page
     */
    public static $URL = '/';

    /**
     * @var StripchatTester;
     */
    protected $I;

    /**
     * @var string $modelName [CSS] name of the model
     */
    public static $modelName = 'div.room-name a';

    /**
     * @var string $modelProfileBtn [CSS] profile button
     */
    public static $modelProfileBtn = 'a#viewcam_aside_profile_link';

    /**
     * @var string $modelProfileCard [CSS] model info card div
     */
    public static $modelProfileCard = 'div[data-component-name="UserInfo"]';

    /**
     * @var string $addToFavoritesBtn [CSS] add to favorites button
     */
    public static $addToFavoritesBtn = 'button[data-component-name="FavoriteButton"]';

    /**
     * Receives actor
     *
     * @param StripchatTester $I
     */
    public function __construct(StripchatTester $I)
    {
        $this->I = $I;
    }

    /**
     * Click to open model profile card and waits
     * for JS to load
     *
     * @return $this
     */
    public function openModelProfile()
    {
        $I = $this->I;
        $I->click(self::$modelProfileBtn);
        $I->waitForElement(self::$modelProfileCard);
        return $this;
    }

    /**
     * Click to add model to the favorites list and waits
     * for JS to load
     *
     * @return $this
     */
    public function addModelToFavorites()
    {
        $I = $this->I;
        $I->click(self::$addToFavoritesBtn);
        $I->waitForText('Удалить из избранного', 5, self::$addToFavoritesBtn);
        return $this;
    }

}
