<?php
namespace Wisetest\Page\Stripchat;

use Wisetest\StripchatTester;

class UserFavoritesPage
{
    /**
     * @var string $URL of the page
     */
    public static $URL = '/favorites';

    /**
     * @var StripchatTester;
     */
    protected $I;

    /**
     * @var string $editAndSaveFavBtn [CSS] This button performs save and edit model list entities
     */
    public static $editAndSaveFavBtn = 'a[data-component-name="EditButton"]';

    /**
     * @var string $removeCrossBtn [CSS] The buttons removing model from list (but you should save via edit button)
     */
    public static $removeCrossBtn = 'div[data-component-name="InfiniteScroll"] span.remove-button';

    /**
     * @var string $modelCardName [CSS] All model names in one locator
     */
    public static $modelCardName = 'div.info > div.name';

    /**
     * @var string $crossesBtn [CSS] All crosses buttons
     */
    public static $crossesBtn = '(//span[@class="remove-button"])';

    /**
     * Receives actor
     *
     * @param StripchatTester $I
     */
    public function __construct(StripchatTester $I)
    {
        $this->I = $I;
    }



}
