<?php
namespace Wisetest\Page\Stripchat;

use Wisetest\StripchatTester;

class LoginPage
{
    /**
     * @var string $URL of the page
     */
    public static $URL = '/login';

    /**
     * @var StripchatTester;
     */
    protected $I;

    /**
     * @var string $usernameField [Xpath] login field locator
     */
    public static $usernameField = '//input[@type="text" and contains(@id, "login")]';

    /**
     * @var string $passwordField [Xpath] password field locator
     */
    public static $passwordField = '//input[@type="password" and contains(@id, "password")]';

    /**
     * @var string $loginBtn [CSS] login button locator
     */
    public static $loginBtn = 'button[type="submit"]';

    /**
     * Receives actor
     *
     * @param StripchatTester $I
     */
    public function __construct(StripchatTester $I)
    {
        $this->I = $I;
    }

    /**
     * Authorize user as login and password,
     * provides fluent interface
     *
     * @param string $login
     * @param string $password
     * @return MainPage
     */
    public function authAs($login, $password)
    {
        $I = $this->I;
        $I->fillField(self::$usernameField, $login);
        $I->fillField(self::$passwordField, $password);
        $I->click(self::$loginBtn);
        return new MainPage($I);
    }

}
