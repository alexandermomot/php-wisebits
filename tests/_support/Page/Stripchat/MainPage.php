<?php
namespace Wisetest\Page\Stripchat;

use Wisetest\StripchatTester;
use Wisetest\Page\Stripchat\ModelPage;

class MainPage
{
    /**
     * @var string $URL of the page
     */
    public static $URL = '/';

    /**
     * @var StripchatTester;
     */
    protected $I;

    /**
     * @var string $usernameTopMenu [XPath] top user menu button
     */
    public static $usernameTopMenu = "//div[@data-component-name='HeaderUserMenu']/a[text()]";

    /**
     * @var string $favoritesBtn [XPath] top menu favorites button
     */
    public static $favoritesBtn = '//a[@href="/favorites"]';

    /**
     * @var string $disclaimerAgreeBtn [XPath] Disclaimer agree button
     */
    public static $disclaimerAgreeBtn = '//button[text()="Мне больше 18 лет"]';

    /**
     * @var string $modelPreview [XPath] locating to all windows with models
     */
    public static $modelPreview = '//div[@data-component-name="ModelListItem"]//a';

    /**
     * Receives actor
     *
     * @param StripchatTester $I
     */
    public function __construct(StripchatTester $I)
    {
        $this->I = $I;
    }

    /**
     * Simply waits until user becomes logged in and the name
     * appears in the top-right corner element
     *
     * @return $this
     */
    public function waitForUserBecomesLoggedIn()
    {
        $I = $this->I;
        $I->waitForText($I->getCredentials()['login'], 15, self::$usernameTopMenu);
        return $this;
    }

    /**
     * Waits for model list to appear
     *
     * @return $this
     */
    public function waitForModelsToAppear()
    {
        $I = $this->I;
        $I->waitForElement(self::$modelPreview);
        $I->waitForElementVisible(self::$modelPreview);
        return $this;
    }


    /**
     * Click link to enter the model page and wait for
     * profile element becomes visible
     *
     * @param string $model absolute name like "/kisa123"
     * @return ModelPage
     */
    public function gotoModelChatPage($model)
    {
        $I = $this->I;
        $I->click(self::$modelPreview . '[@href="' . $model . '"]');
        $I->waitForElement("//a[@href='/user${model}']");
        return new ModelPage($I);
    }

}
