<?php
namespace Wisetest;

use Wisetest\Page\Market\MainPage;
use Wisetest\Page\Market\CatalogPage;
use Codeception\Module\WebDriver;
use Codeception\Util\Debug;
use Yandex\Allure\Adapter\Annotation\Issues;

class YandexMarketSearchCest
{
    /** @var WebDriver */
    private $wd;

    /**
     * Test data examples (without dataprovider)
     */
    const DATA = [
        'vendor' => 'Nokia',
        'category' => 'мобильные телефоны',
        'model' => 'Nokia Lumia 520',
        #'model' => 'Nokia Lumia 520 White',  # unit isn't exist
    ];

    /**
     * Injecting actor's driver object
     * @param WebDriver $webDriver
     */
    public function _inject(WebDriver $webDriver)
    {
        $this->wd = $webDriver;
    }

    /**
     * Performs basic action of open required page in the web browser
     * @param MarketTester $I
     */
    public function _before(MarketTester $I)
    {
        $I->amGoingTo('open main page');
        $I->amOnPage(MainPage::$URL);
    }

    /**
     * @Issues("TEST-1")
     * @group ui
     *
     * @param MarketTester $I
     */
    public function checkNokiaProductExistence(MarketTester $I)
    {
        $I->wantToTest("existence of the Nokia phone on the page");

        $I->amGoingTo('type model name into the search field');
        (new MainPage($I))
            ->searchFor(self::DATA['vendor'])
            ->waitForPopupMenuShown()
            ->clickPopupMenuItemLike(self::DATA['category']);
        $I->seeInCurrentUrl(CatalogPage::$URL);

        $I->amGoingTo('check that required model exist on the current page');
        $this->lookUpPagesAndFind($I, self::DATA['model'], 5);
    }

    /**
     * @param MarketTester $I
     * @param string $product name
     * @param int $pages number to lookup (setup threshold to limit cycle)
     */
    protected function lookUpPagesAndFind($I, $product, $pages = 10)
    {
        $i = 0;
        $catalogPage = new CatalogPage($I);
        Debug::debug("Looking for a phone in the next $pages pages");
        while (true) {
            $nextButton = $this->wd->_findElements(CatalogPage::$nextPageBtn);

            if ($i < $pages) {
                Debug::debug("Looking for ${product} on the page: #" . ++$i);
            } else {
                Debug::debug("Page limit reached, exiting... ");
                break;
            }

            $phoneNames = $I->grabMultiple(CatalogPage::$productTitle);

            if (in_array($product, $phoneNames)) {
                $catalogPage->scrollToProductView($product);
                $I->makeScreenshot('phone-found');
                $I->see($product);
                return;
            }

            if (!empty($nextButton)) {
                Debug::debug("Product isn't found on the current page...");
                $catalogPage
                    ->clickNextButton()
                    ->waitPageToLoad();
                continue;
            } else {
                Debug::debug("There is no 'next page' button found, exiting... ");
                break;
            }
        }
        $I->makeScreenshot('phone-not-found');
        $I->fail("Last page reached, product ${product} wasn't found");
    }
}
