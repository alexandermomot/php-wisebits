<?php
namespace Wisetest;

use Wisetest\GmailTester;
use Wisetest\Page\Gmail\LoginPage;
use Wisetest\Page\Gmail\IncomingPage;
use Codeception\Module\WebDriver;
use Codeception\Util\Debug;
use Yandex\Allure\Adapter\Annotation\Issues;

class BobAndAliceCest
{
    /** @var \Codeception\Lib\Friend $alice */
    private $alice;

    /** @var array $config user credentials */
    private $config;

    /** @var string $currTime current human-readable time format */
    private $currTime;

    /**
     * Main goal is to define a friend of the bob
     * @param \Wisetest\GmailTester $bob
     */
    public function _before(GmailTester $bob)
    {
        $this->config = $this->_getCredentials();

        $bob->am('Bob');
        $alice = $bob->haveFriend('Alice', GmailTester::class);
        $this->alice = $alice;
        $config = $this->config;

        $time = new \DateTime();
        $this->currTime = $time->format('D M j G:i:s T Y');

        $bob->amGoingTo('open main page');
        $bob->amOnPage(LoginPage::$URL);

        (new LoginPage($bob))
            ->loginAs($config['bob']['login'], $config['bob']['password']);

        $authAliceCallBack =
            function(GmailTester $alice) use ($config)
            {
                $alice->am('Alice');
                $alice->amGoingTo('open gmail.com too');
                $alice->amOnPage(LoginPage::$URL);
                (new LoginPage($alice))
                    ->loginAs($config['alice']['login'], $config['alice']['password']);
            };

        $alice->does($authAliceCallBack);
    }

    /**
     * @Issues("TEST-2")
     * @group ui
     *
     * @param \Wisetest\GmailTester $bob
     */
    public function checkDialogBetweenBobAndAlice(GmailTester $bob)
    {
        $bob->wantToTest('gmail, send mail to Alice and to receive answer');
        $bob->amGoingTo('send letter to Alice');
        $alice = $this->alice;
        $mailboxBob = new IncomingPage($bob);

        $bobToAlice = [
            'subject' => 'What time is it now? ' . rand(111,999),
            'body' => 'I think it is: ' . $this->currTime,
            'reply' => 'I see, thank you Alice!'
        ];

        $aliceToBob = [
            'subject' => 'That is correct, Bob',
            'body' => 'True, Bob, the time is: ' . $this->currTime,
        ];

        // Bob sends mail to Alice
        $mailboxBob
            ->createNewLetter()
            ->inputWho($this->config['alice']['login'])
            ->inputSubject($bobToAlice['subject'])
            ->inputLetter($bobToAlice['body'])
            ->sendLetter();

        $this->alice->isGoingTo('check that letter is here');

        // Alice read the mail and reply to Bob
        $alice->does(
            function(GmailTester $alice) use ($bobToAlice, $aliceToBob) {
                $alice->amGoingTo('check email and find letter from bob');
                $mailboxAlice = new IncomingPage($alice);

                $mailboxAlice
                    ->checkInbox()
                    ->waitForLetterWith($bobToAlice['subject'], $bobToAlice['body']);

                $alice->see($bobToAlice['subject']);

                $mailboxAlice
                    ->openLetter()
                    ->replyWithText($aliceToBob['body']);
            });

        // Bob waiting for a letter from Alice
        $mailboxBob
            ->checkInbox()
            ->waitForLetterWith('Re: ' . $bobToAlice['subject'], $aliceToBob['body'])
            ->openLetter();

        // Bob see the message arrived and checking it contents, send reply back to Alice
        $bob->see($aliceToBob['body']);
        $mailboxBob
            ->replyWithText($bobToAlice['reply']);

        // Alice waits for a letter with the reply text from Bob
        $alice->does(
            function(GmailTester $alice) use ($bobToAlice, $aliceToBob) {
                (new IncomingPage($alice))
                    ->checkInbox()
                    ->waitForLetterWith('Re: ' . $bobToAlice['subject'], $bobToAlice['reply'])
                    ->openLetter();
                $alice->see($bobToAlice['reply']);
                $alice->makeScreenshot('alice-see-letter-from-bob');
            });

        $bob->makeScreenshot('bob-see-letter-from-alice');
    }

    /**
     * Here we will leave bob and alice alone,
     * sadly
     * @param \Wisetest\GmailTester $bob
     */
    public function _after(GmailTester $bob)
    {
        $bob->amGoingTo('say: thank you and bye-bye Alice!');
        $this->alice->leave();
    }

    /**
     * Pre-setup
     */
    protected function _getCredentials()
    {
        return [
            'bob'   => ['login' => getenv('BOB_LOGIN'),   'password' => getenv('BOB_PASSWORD')],
            'alice' => ['login' => getenv('ALICE_LOGIN'), 'password' => getenv('ALICE_PASSWORD')]
        ];
    }
}
